use swayipc::{Connection, Fallible};

#[inline(always)]
fn get_workspace_number(workspaces: &Vec<swayipc::Workspace>) -> i32 {
    let last = workspaces
        .into_iter()
        .map(|w| w.num )
        .reduce(|n1, n2| std::cmp::max(n1, n2))
        .unwrap();
    if last < 0 { 1 } else { last + 1 }
}

fn main() -> Fallible<()> {
    let mut connection = Connection::new()?;
    let workspaces = connection.get_workspaces()?;

    let focused: Vec<_> = workspaces
        .iter()
        .filter(|wrkspc| wrkspc.focused)
        .collect();
    
    if !focused.is_empty() && focused[0].representation != None {
        let workspace_number = get_workspace_number(&workspaces);
        connection.run_command(format!("workspace {}", workspace_number))?;
    }
    Ok(())
}
