# RainDx's Desktop Utilities (rddutils)
A collection of small utilities I use on my desktop

## sway_add_workspace
Switch to a new (numbered) workspace on sway

## waybar_sensors
displays sensors values in waybar
