use libmedium::{
    hwmon::Hwmon,
    parse_hwmons,
    sensors::{temp::TempSensor, Sensor},
};

use waybar_custom::WaybarOutput;

/// Returns the index of the hwmon coretemp
/// if it is not found it falls back to acpitz and then to the first
///
/// # Arguments
///
/// * `hwmons` - A Hwmon vector
///
fn get_main_hwmon_idx(hwmons: &Vec<Hwmon>) -> u16 {
    debug_assert!(hwmons.len() > 0);
    let (mut index, mut i): (usize, usize) = (0, 0);
    let mut coretemp_found = false;
    while !coretemp_found && i < hwmons.len() {
        match hwmons[i].name() {
            "coretemp" => {
                index = i;
                coretemp_found = true;
            }
            "acpitz" => index = i,
            &_ => {}
        }
        i += 1;
    }
    hwmons[index].index()
}

/// Returns a vector containing all the temperature-related hwmons
fn get_temp_hwmons() -> Vec<Hwmon> {
    let mut temp_hwmons = Vec::new();
    (&parse_hwmons().unwrap()).into_iter().for_each(|hwmon| {
        if hwmon.temps().len() > 0 {
            temp_hwmons.push(hwmon.to_owned());
        }
    });
    temp_hwmons
}

fn main() {
    let hwmons = get_temp_hwmons();
    let mut text = String::new();
    let mut tooltip = String::new();
    let mut percentage: Option<u32> = Some(50);
    let main_idx = get_main_hwmon_idx(&hwmons);
    hwmons.into_iter().for_each(|hwmon| {
        if hwmon.index() == main_idx {
            for (_, temp_sensor) in hwmon.temps() {
                if text.is_empty() {
                    let input = match temp_sensor.read_input() {
                        Ok(temp) => temp.as_degrees_celsius(),
                        Err(_) => 0.0_f64
                    };
                    let max = match temp_sensor.read_crit() {
                        Ok(temp) => temp.as_degrees_celsius(),
                        Err(_) => match temp_sensor.read_crit() {
                            Ok(temp) => temp.as_degrees_celsius(),
                            Err(_) => 100.0_f64
                        }
                    };
                    text = (input.round() as u32).to_string();
                    percentage = Some((100.0 * (input / max)) as u32);
                }
            }
        }
        tooltip = format!("{}{}\n", tooltip, hwmon.name());
        //println!("hwmon{} with name {}:", hwmon.index(), hwmon.name());
        for (_, temp_sensor) in hwmon.temps() {
            let temperature = temp_sensor.read_input().unwrap();
            tooltip = format!("{}\t{}: {}\n", tooltip, temp_sensor.name(), temperature);
        }
    });
    let mut ouput = WaybarOutput::new(text);
    ouput.tooltip = Some(tooltip);
    ouput.percentage = percentage;
    println!("{}", ouput.to_json().unwrap_or("{}".to_string()));
}
