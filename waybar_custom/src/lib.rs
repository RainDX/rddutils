use serde::{Serialize, Deserialize};
use html_escape;

#[derive(Serialize, Deserialize, Debug)]
pub struct WaybarOutput {
   pub text: String,
   pub alt: Option<String>,
   pub tooltip: Option<String>,
   pub percentage: Option<u32>,
}

impl WaybarOutput {
    pub fn new(text: String) -> WaybarOutput {
        let mut safe_text = String::new();
        html_escape::encode_safe_to_string(text, &mut safe_text);
        WaybarOutput { text: safe_text, alt: None, tooltip: None, percentage: None }
    }
    pub fn set_text(&mut self, text: String) {
        let mut safe_text = String::new();
        html_escape::encode_safe_to_string(text, &mut safe_text);
        self.text = safe_text;
    }

    pub fn set_alt(&mut self, text: String) {
        let mut safe_text = String::new();
        html_escape::encode_safe_to_string(text, &mut safe_text);
        self.alt = Some(safe_text);
    }

    pub fn set_tooltip(&mut self, text: String) {
        let mut safe_text = String::new();
        html_escape::encode_safe_to_string(text, &mut safe_text);
        self.tooltip = Some(safe_text);
    }

    pub fn to_json(&self) -> Result<String,serde_json::Error> {
        serde_json::to_string(self)
    }

    pub fn from_json(json: &str) -> Result<WaybarOutput,serde_json::Error> {
        serde_json::from_str(json)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn serialisation() {
        let o = WaybarOutput::new("text".to_string());
        let result = o.to_json();
        assert_eq!(result.unwrap(), "{\"text\":\"text\",\"alt\":null,\"tooltip\":null,\"percentage\":null}");
    }

    #[test]
    fn deserialisation() {
        let o = "{\"text\":\"text\",\"alt\":null,\"tooltip\":null,\"percentage\":null}";
        let result = WaybarOutput::from_json(o).unwrap();
        assert_eq!(result.text, "text");
        assert_eq!(result.alt, None);
        assert_eq!(result.tooltip, None);
        assert_eq!(result.percentage, None);
    }

    #[test]
    fn serialisation_full() {
        let mut o = WaybarOutput::new("text".to_string());
        o.alt = Some("alt".to_string());
        o.tooltip = Some("tooltip".to_string());
        o.percentage = Some(42);
        let result = o.to_json();
        assert_eq!(result.unwrap(),"{\"text\":\"text\",\"alt\":\"alt\",\"tooltip\":\"tooltip\",\"percentage\":42}");
    }

    #[test]
    fn deserialisation_full() {
        let o = "{\"text\":\"text\",\"alt\":\"alt\",\"tooltip\":\"tooltip\",\"percentage\":42}";
        let result = WaybarOutput::from_json(o).unwrap();
        assert_eq!(result.text, "text");
        assert_eq!(result.alt, Some("alt".to_string()));
        assert_eq!(result.tooltip, Some("tooltip".to_string()));
        assert_eq!(result.percentage, Some(42));
    }
}
