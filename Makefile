CARGO = cargo

CARGO_OPTS =

ifeq ($(DEBUG), 1)
    BUILD_FLAGS=
else
    BUILD_FLAGS=--release
endif

ifeq ($(PREFIX),)
    PREFIX := $(HOME)/.local/
endif

all:
	$(MAKE) build

build:
	$(CARGO) $(CARGO_OPTS) build $(BUILD_FLAGS)

clean:
	$(CARGO) $(CARGO_OPTS) clean

install: 
	$(CARGO) $(CARGO_OPTS) install --path sway_add_workspace --root $(PREFIX)
	$(CARGO) $(CARGO_OPTS) install --path waybar_sensors --root $(PREFIX)
	$(CARGO) $(CARGO_OPTS) install --path waybar_player --root $(PREFIX)

.PHONY: all build install install
