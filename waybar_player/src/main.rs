use mpris::{Player, PlayerFinder};
use waybar_custom::WaybarOutput;

fn append_metadata(player: &Player, tooltip: &mut String) {
    match player.get_metadata() {
        Ok(metadata) => {
            tooltip.push_str(metadata.title().unwrap_or(""));
            match metadata.album_name() {
                Some(album) => {
                    tooltip.push_str(" | ");
                    tooltip.push_str(album);
                }
                None => {}
            }
            match metadata.artists() {
                Some(artists) => {
                    tooltip.push_str(" by ");
                    for artist in artists {
                        tooltip.push_str(artist);
                    }
                }
                None => match metadata.album_artists() {
                    Some(artists) => {
                        tooltip.push_str(" by ");
                        for artist in artists {
                            tooltip.push_str(artist);
                        }
                    }
                    None => {}
                },
            }
        }

        Err(_) => {}
    }
}

fn append_playing_status(player: &Player, text: &mut String) {
    match player
        .get_playback_status()
        .unwrap_or_else(|_| mpris::PlaybackStatus::Stopped)
    {
        mpris::PlaybackStatus::Playing => text.push_str(" "),
        mpris::PlaybackStatus::Paused => text.push_str(" "),
        mpris::PlaybackStatus::Stopped => text.push_str(" "),
    }
}

fn set_text_active_player(player_finder: &PlayerFinder, text: &mut String) {
    match player_finder.find_active() {
        Ok(player) => {
            append_playing_status(&player, text);
            match player.get_metadata() {
                Ok(metadata) => {
                    text.push_str(metadata.title().unwrap_or(""));
                }
                Err(_) => {}
            }
        }
        Err(_) => {
            text.clear();
        }
    }
}

fn set_tooltip(player_finder: &PlayerFinder, tooltip: &mut String) {
    match player_finder.find_all() {
        Ok(players) => {
            for player in &players {
                if tooltip.is_empty() {
                    tooltip.push_str(player.identity());
                } else {
                    tooltip.push_str("\n");
                    tooltip.push_str(player.identity());
                }
                tooltip.push_str(": ");
                append_playing_status(&player, tooltip);
                append_metadata(player, tooltip);
            }
        }
        Err(_) => {
            tooltip.clear();
        }
    }
}

fn main() {
    let mut text = String::new();
    let mut tooltip = String::new();

    let player_finder = PlayerFinder::new().expect("Could not connect to D-Bus");

    set_text_active_player(&player_finder, &mut text);
    set_tooltip(&player_finder, &mut tooltip);

    let mut ouput = WaybarOutput::new(text);
    ouput.set_tooltip(tooltip);
    println!("{}", ouput.to_json().unwrap_or("{}".to_string()));
}
